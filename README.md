Kinalite Example
================

This project serves as a basic example for using the `kinalite` library to analyze experiment data.

The main script is `main.py` which loads experiment data from the `data` folder and analyzes it with `kinalite`.

You can work with your own data by updating the CSV filepaths near the top of `main.py`.