import pandas as pd
from kinalite.experiment import Experiment
from kinalite.plots import plot_experiment_results

# first, use pandas to import CSV or Excel files, the first column MUST be time
data_ref = pd.read_csv('./data/perfect_data/exp1_ref.csv')
data_a = pd.read_csv('./data/perfect_data/exp2_xs_a.csv')
data_b = pd.read_csv('./data/perfect_data/exp3_xs_b.csv')
data_b_and_cat = pd.read_csv('./data/perfect_data/exp4_xs_b_and_cat.csv')

# Create new Experiment instances to analyze the experiment data. Each instance requires a name,
# 2 sets of data, along with the indexes of the substrate and product columns in the data (these start at 0).

# find the order in A, using our ref and A data
# The substrate_index is 2 (the "A" column) and the product_index is 4 (the "D" column)
experiment_a = Experiment('A', [data_ref, data_a], substrate_index=2, product_index=4)

# find the order in B, using our ref and B data
# The substrate_index is 3 (the "B" column) and the product_index is 4 (the "D" column)
experiment_b = Experiment('B', [data_ref, data_b], substrate_index=3, product_index=4)

# find the order in cat, using our ref and cat data
# The substrate_index is 5 (the "cat" column) and the product_index is 4 (the "D" column)
experiment_cat = Experiment('cat', [data_b, data_b_and_cat], substrate_index=5, product_index=4)


# now we run `calculate_best_results` for each experiment
experiment_a_result = experiment_a.calculate_best_result()
experiment_b_result = experiment_b.calculate_best_result()
experiment_cat_result = experiment_cat.calculate_best_result()

print(f"""
Results:
--------

Order in A: {experiment_a_result.order}
Order in B: {experiment_b_result.order}
Order in cat: {experiment_cat_result.order}
""")

# you can use the `plot_experiment_results` method to generate plots of the analyzed orders and their scores,
# as well as plots of the overlapping time-normalized data
plot_experiment_results(experiment_a)
plot_experiment_results(experiment_b)
plot_experiment_results(experiment_cat)
